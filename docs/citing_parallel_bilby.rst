=======================================
Acknowledging/Citing Parallel Bilby
=======================================

If you have used Parallel Bilby in your scientific work we would appreciate it if you would acknowledge it. The continued growth and development of Parallel Bilby is dependent on the community being aware of Parallel Bilby.

Publications
------------
Please add the following line within your methods, conclusion or acknowledgements
sections:

    "This research has made use of Parallel Bilby vX.Y (version citation), an
    open-source and free community-developed parallelised Bayesian inference Python
    package (citation)."

.. code:: bibtex

    @article{smith2019expediting,
      title={Expediting Astrophysical Discovery with Gravitational-Wave Transients Through Massively Parallel Nested Sampling},
      author={Smith, Rory and Ashton, Gregory},
      journal={arXiv preprint arXiv:1909.11873},
      year={2019}
    }


The citation should be to the `Parallel Bilby paper`_ and the version number should
cite the specific version used in your work.


Posters and talks
------------------
Please include the `Parallel Bilby logo`_ on the title, conclusion slide, or about page.


.. _Parallel Bilby paper: https://arxiv.org/pdf/1909.11873.pdf
.. _Parallel Bilby logo: https://git.ligo.org/uploads/-/system/project/avatar/1846/bilby.jpg?width=40
